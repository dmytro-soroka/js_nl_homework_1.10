
//PS: Стили всех элементов остаются на ваш вкус и цвет

/* 
    TASK 1

    На странице html у нас имеется разметка:

    <header>
      <nav>
        <ul>
          <li>
            <a>Home</a>
          </li>
          <li>
            <a>News</a>
          </li>
          <li>
            <a>Contacts</a>
          </li>
        </ul>
      </nav>
    </header>

    Необходимо добавить с ПОМОЩЬЮ JAVASCRIPT в навигацию по сайту следующие разделы :

    - About Us
    - Our Team

    Первый раздел должен находится перед News

    Второй перед Contacts

    Единственные правки в html которые вы можете внести, это добавить атрибуты элементам

*/

/* 
    TASK 2
    На странице html у нас имеется разметка:
    <header>
      <nav>
        <ul>
          <li>
            <a>Home</a>
          </li>
          <li>
            <a>News</a>
          </li>
          <li>
            <a>Contacts</a>
          </li>
        </ul>
      </nav>
    </header>
  Необходимо ПОЛНОСТЬЮ скопировать навигацию из шапки в футер, при этом не касаясь файла html.

  outcome of task :

  Две одинаковых навигации и в шапке и в футере
*/

/* 
    TASK 3

    На странице HTML мы имеем:

    <button>Open the first block</button>

    <button>Open the second block</button>

    <button>Open the third block</button>

    <div class="block">First Block</div>

    <div class="block">Second Block</div>

    <div class="block">Third Block</div>

    все элементы с классом "block" по умолчанию скрыты

    Задача:

    - при нажатии на первую кнопку пользователь может открывать/закрывать First Block

    - при нажатии на вторую кнопку пользователь может открывать/закрывать Second Block

    - при нажатии на третью кнопку пользователь может открывать/закрывать Third Block
    
*/

/* 
    TASK 4

    Имеем блок 300px x 300px, внутри него много текста который не помещается в него

    Соответсвенно у этого блока добавляем стиль overflow:scroll

    В результате мы должны получить блок с полосой прокрутки и возможностью увидеть весь текст когда мы скроллим блок

    У нас есть рядом с блоком с текстом кнопка, при нажатии на которую мы всегда отправляемся в начало нашего текста

    Эта кнопка по умолчанию скрыта, и появляется только тогда когда пользователь проскролит до 100px

    Когда пользователь оказывается вверху блока(в начале текста), кнопка снова должна скрываться
    
*/

/* 
    TASK 5

    У нас есть html форма:

    <form action="">

      <label for="">FirstName

        <input type="text">

      </label>

      <label for="">LastName

        <input type="text">

      </label>

      <label for="">Phone Number

        <input type="text">

      </label>

      <button>Send the form</button>

    </form>

    1. При вводе имени "Sergey" в input отвечающий за имя, пользователь должен видеть сообщение в alert окне: "Имя как у нашего преподователя"

    2. При вводе фамилии "Eremenko" в input отвечающий за фамилию, пользователь должен видеть сообщение в alert окне: "Фамилия как у нашего наставника"

    3. Создайте пустой div на странице, и при отправке формы данные из трех input должны выводиться в этом самом div, в формате:

    Спасибо за отправку формы, ваши данные:

    FirstName : 'значение из input отвечающий за FirstName'

    LastName : значение из input отвечающий за LastName

    PhoneNumber : значение из input отвечающий за PhoneNumber

    Перед отправкой формы этот div должен быть пустым

*/





